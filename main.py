import openpyxl

# This function will load the file
inventory_file = openpyxl.load_workbook("inventory.xlsx")
product_list = inventory_file["Sheet1"]

#Task 1: calculate how many product do we have per supplier and list the product
#since we have three supplier so we will end up with three supplier dict
no_product_per_supplier = {}   #1st dict
Total_value_per_supplier = {}   #2nd dict
product_under_10_inventory = {}  #3rd dict

# we need a loop through every line in the spreadsheet
# number of time will this loop run "any number of product in the file"
#Test the max_row functionality like: should be 75 as per the spreadsheet
#print(product_list.max_row)
#List is starting from row 2 so therefore we SET 2 in range to from as below: Start point = 2nd row --> End point = max_row
# Another issue = Last line will not be included so will do +1 to reach to last row

for product_row in range(2, product_list.max_row +1):
    # this will give us a supplier name
    supplier_name = product_list.cell(product_row, 4).value
    inventory = product_list.cell(product_row, 2).value
    price = product_list.cell(product_row, 3).value
    product_num = product_list.cell(product_row, 1).value
    inventory_price = product_list.cell(product_row, 5) # task 4

      #next we need to build dict:
    if supplier_name in no_product_per_supplier:
        current_num_product = no_product_per_supplier.get(supplier_name)
        no_product_per_supplier[supplier_name] = current_num_product + 1
    else:
        #print("adding a new supplier")
        no_product_per_supplier[supplier_name] = 1

#Tasks 2: Which company we had the highest product && Value of inventory per supplier
# We will have to create a dict
    if supplier_name in Total_value_per_supplier:
        current_total_value = Total_value_per_supplier.get(supplier_name)
        Total_value_per_supplier[supplier_name] = current_total_value + (inventory * price)
    else:
        Total_value_per_supplier[supplier_name] = inventory * price

#Task 3: Print out the product has inventory < 10
# We will need dict again
    if inventory < 10:
        product_under_10_inventory[int(product_num)] = int(inventory)


#Task 4: Assigning some value/colum in spreadsheet
#This will add the value into the colum as total BUT NOT save
    inventory_price.value = inventory * price

#printout everything what we have done so far
print(no_product_per_supplier)
print(Total_value_per_supplier)
print(product_under_10_inventory)

# Below will SAVE a file and create a new file
inventory_file.save("Save file with total inventory value.xlsx")
# New file has been created:C:\Users\Arslan.Shahid\PycharmProjects\python_spreadsheet_project\Save file with total inventory value.xlsx

#Print the output
print(no_product_per_supplier)
print(Total_value_per_supplier)
# print sequence: product num: product count
print(f"List of product < 10: {product_under_10_inventory}")




