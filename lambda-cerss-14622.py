import json
import boto3


print('Loading function')


def lambda_handler(event, context):
    message = event['Records'][0]['Sns']['Message']
    messagedict = json.loads(message)
    ec2instanceid = messagedict['EC2InstanceId']
    #print(ec2instanceid)
    # Create CloudWatch client
    cloudwatch = boto3.client('cloudwatch')
    
    event = messagedict['Event']
    if event == "autoscaling:EC2_INSTANCE_TERMINATE":
        print('instance_terminating '+ ec2instanceid)
        cloudwatch.delete_alarms(
        AlarmNames=['cpu_utilization '+ ec2instanceid])
        cloudwatch.delete_alarms(
        AlarmNames=['mem_used_percent '+ ec2instanceid])
        cloudwatch.delete_alarms(
        AlarmNames=['disk_used_percent '+ ec2instanceid])
        #print("Terminating")
    elif event == "autoscaling:EC2_INSTANCE_LAUNCH":
        #print("Launching")
        cloudwatch.put_metric_alarm(
        AlarmName='cpu_utilization '+ ec2instanceid,
        ComparisonOperator='GreaterThanThreshold',
        EvaluationPeriods=1,
        MetricName='cpu_utilization',
        Namespace='AWS/EC2',
        Period=300,
        Statistic='Average',
        Threshold=85,
        ActionsEnabled=True,
        AlarmDescription='Alarm when server cpu_utilization exceeds 85%',
        Dimensions=[
            {
              'Name': 'InstanceId',
              'Value': ec2instanceid
            },
        ],
        Unit='Seconds'
    )   
        cloudwatch.put_metric_alarm(
        AlarmName='mem_used_percent '+ ec2instanceid,
        ComparisonOperator='GreaterThanThreshold',
        EvaluationPeriods=1,
        MetricName='mem_used_percent',
        Namespace='AWS/EC2',
        Period=300,
        Statistic='Average',
        Threshold=90,
        ActionsEnabled=True,
        AlarmDescription='Alarm when server mem_used_percent exceeds 90%',
        Dimensions=[
            {
              'Name': 'InstanceId',
              'Value': ec2instanceid
            },
        ],
        Unit='Seconds'
    )
        cloudwatch.put_metric_alarm(
        AlarmName='disk_used_percent '+ ec2instanceid,
        ComparisonOperator='GreaterThanThreshold',
        EvaluationPeriods=1,
        MetricName='disk_used_percent',
        Namespace='AWS/EC2',
        Period=300,
        Statistic='Average',
        Threshold=90,
        ActionsEnabled=True,
        AlarmDescription='Alarm when server disk_used_percent exceeds 90%',
        Dimensions=[
            {
              'Name': 'InstanceId',
              'Value': ec2instanceid
            },
        ],
        Unit='Seconds'
    )